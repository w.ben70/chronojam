using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public float keyTimer;
    public string code;
    public bool keyHeld;
    bool enable;
    ComboManager comboManager;
    UIController uiController;

    // Start is called before the first frame update
    void Start()
    {
        comboManager = GameObject.Find("ComboManager").GetComponent<ComboManager>();
        uiController = GameObject.Find("UI").GetComponent<UIController>();
        keyTimer = 0;
        keyHeld = false;
        enable = false;
        code = "a"; // bad code for invalid
    }

    void releaseKey() {
        // Debug.Log(code + " Key released in " + keyTimer + " seconds");
        if (keyHeld)
            comboManager.AddInput(code, keyTimer);

        keyHeld = false;
        uiController.SetButtonReleased(code);
    }

    void pressKey(string code) {
        keyTimer = 0;
        this.code = code;
        keyHeld = true;
        /// Debug.Log(code + " Key pressed");
        comboManager.resetComboTimer();
        uiController.SetButtonPressed(code);
    }

    // Update is called once per frame
    void Update()
    {
        if (!enable) {
            return;
        }
    
        // New key pres
        // Sorry, this is redundant but I don't care

        if (Input.GetKeyDown("q"))
        {
            releaseKey();
            pressKey("q");
        }
        else if (Input.GetKeyDown("w"))
        {
            releaseKey();
            pressKey("w");
        }
        else if (Input.GetKeyDown("e"))
        {
            releaseKey();
            pressKey("e");
        }

        // Key is still being held
        if (keyHeld && Input.GetKey(code)) {
            keyTimer += Time.deltaTime;
        // Key was released
        } else if (Input.GetKeyUp(code)) {
            releaseKey();
        }
    }

    public void SetInputEnable(bool enable) {
        this.enable = enable;
    }
}
