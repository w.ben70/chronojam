using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MainMenuControl : MonoBehaviour
{
    public GameObject UI;
    public UIDocument uiControl;
    public GameObject ScoreManager;
    // Start is called before the first frame update
    void Start()
    {
        UI = GameObject.Find("UI");
        uiControl = GetComponent<UIDocument>();
        ShowStartGameText();
        Time.timeScale = 0;
        GameObject.Find("InputManager").GetComponent<InputManager>().SetInputEnable(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q)) {
            Time.timeScale = 1;
            UI.SetActive(true);
            GameObject.Find("InputManager").GetComponent<InputManager>().SetInputEnable(true);
            ShowStartGameText();
            ScoreManager.GetComponent<ScoreManager>().GameRestart();
            this.gameObject.SetActive(false);
        }
        else if (Input.GetKeyDown(KeyCode.W)) {
            //executable build quit
            Application.Quit();
            //Editor build quit --comment out for build
            //UnityEditor.EditorApplication.isPlaying = false;
        }
    }

    void ShowStartGameText() {
        var root = GetComponent<UIDocument>().rootVisualElement;
        root.Q<Label>("ScoreValue").text = "0000";
        root.Q<VisualElement>("GameOver").style.display = DisplayStyle.None;
        root.Q<Button>("Start").text = "Start";
        root.Q<Button>("Start").text = "Start";
        root.Q<Button>("Start").text = "Start";
    }
    public void ShowGameOverText(int score) {
        var root = GetComponent<UIDocument>().rootVisualElement;
        root.Q<VisualElement>("GameOver").style.display = DisplayStyle.Flex;
        root.Q<Label>("ScoreValue").text = score.ToString();
        root.Q<Button>("Start").text = "Restart?";
        GameObject.Find("InputManager").GetComponent<InputManager>().SetInputEnable(false);
        GameOver();
        Time.timeScale = 0;
        //cooldown before starting a new round
    }
    
    void GameOver() {
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
            Destroy(enemy.gameObject);
        }
        GameObject.Find("UI").GetComponent<UIController>().SetEffectiveText("");
        GameObject.Find("UI").GetComponent<UIController>().HideCombo();
    }
}
