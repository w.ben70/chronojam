using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 2f;
    public float slowdown = 4f;
    public float maxHealth = 100f;
    public float health    = 100f;
    public int   points    = 100;
    public bool attackable = false;
    public GameObject healthBar;
    public ScoreManager scoreManager;
    public UIController uiController;
    public string effectiveComboName;
    public string resistComboName;
    public AudioController sfxController;

    void Start ()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        uiController = GameObject.Find("UI").GetComponent<UIController>();
        sfxController = GameObject.Find("SFXplayer").GetComponent<AudioController>();
    }

    //Moves this GameObject 2 units a second in the forward direction
    void FixedUpdate()
    {
        transform.Translate(Vector2.down * Time.deltaTime * speed);
    }

    //Upon collision with another GameObject, this GameObject will reverse direction
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Slowdown") {
            speed = speed / slowdown;
            attackable = true;
        } else if (other.gameObject.tag == "Leak") {
            Debug.Log("Enemy leaked!");
            scoreManager.LoseLife();
            Destroy(gameObject);
        } else if (other.gameObject.tag == "Barrel") {
            other.gameObject.GetComponent<BarrelController>().MoveBarrel();
            Destroy(gameObject);
        } else if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("enemy contact made");
            if (other.gameObject.GetComponent<EnemyController>().health > 0) {
                uiController.SetEffectiveText("Bumped!");
                if (other.gameObject.transform.position.y < this.gameObject.transform.position.y) {
                    other.GetComponent<EnemyController>().speed = 3f;
                }
            }
            
            // todo - player speed up sound
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Slowdown") {
            speed *= slowdown;
            attackable = false;
        }
    }

    public void DealDamage(Combo c) {
        if (c.Name == effectiveComboName) {
            health -= (c.Damage * 2);
            sfxController.playCritHit();
            uiController.SetEffectiveText("2x effective!");
        } else if (c.Name == resistComboName) {
            //health -= (c.Damage / 2); -- negating damage instead
            sfxController.playBadHit();
            uiController.SetEffectiveText("Not effective...");
        } else {
            health -= c.Damage;
            sfxController.playNormalHit();
            uiController.SetEffectiveText("Hit!");
        }

        float ratio = health / maxHealth;
        if (health <= 0f) {
            Debug.Log("Enemy destroyed!");
            scoreManager.AddScore(points);
            attackable = false;
            GetComponent<Rigidbody2D>().velocity = new Vector2(c.xVelocity, c.yVelocity);
            GetComponent<Rigidbody2D>().rotation = c.xVelocity * 2f;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            speed = 0f;
            Destroy(gameObject, 2f);
            healthBar.transform.localScale    = new Vector2(0, 1);
        } else {
            healthBar.transform.localScale    = new Vector2(ratio, 1);
            healthBar.transform.localPosition = new Vector2(-(1-ratio)/2, 0f);
        }
    }
}
