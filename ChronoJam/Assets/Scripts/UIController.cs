using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UIController : MonoBehaviour
{
    UIDocument UI;
    VisualElement[] inputButtons = new VisualElement[3];
    VisualElement heldButton;
    bool buttonHeld;
    float buttonHeldTimer;
    ComboManager comboManager;
    public Color longPressColor;
    public Color buttonDefaultColor;

    public VisualTreeAsset comboContainer;
    public VisualTreeAsset comboButton;

    // Start is called before the first frame update
    void Start()
    {
        UI = GetComponent<UIDocument>();
        var root = UI.rootVisualElement;
        inputButtons[0] = root.Q<VisualElement>("Button1");
        inputButtons[1] = root.Q<VisualElement>("Button2");
        inputButtons[2] = root.Q<VisualElement>("Button3");

        SetButtonReleased("Q");
        SetButtonReleased("W");
        SetButtonReleased("E");

        comboManager = GameObject.Find("ComboManager").GetComponent<ComboManager>();
        HideCombo();
    }

    // Update is called once per frame
    void Update()
    {
        if (buttonHeld) {
            buttonHeldTimer += Time.deltaTime;
            float val = 0.5f - buttonHeldTimer / 4.0f;

            if (buttonHeldTimer > comboManager.longPressTime) {
                heldButton.style.backgroundColor = longPressColor;
            } else {
                heldButton.style.backgroundColor = new StyleColor(new Color(val, val, val, 1));
            }
        }
    }

    public void HideCombo() {
        var root = UI.rootVisualElement;
        root.Q<VisualElement>("ComboTimer").style.display = DisplayStyle.None;
    }

    public void SetComboProgress(float ratio) {
        var root = UI.rootVisualElement;
        root.Q<VisualElement>("ComboTimer").style.display = DisplayStyle.Flex;
        root.Q<VisualElement>("ComboProgress").style.width = Length.Percent(ratio * 100f);
    }

    public void SetShiftProgress(float ratio) {
        var root = UI.rootVisualElement;
        root.Q<VisualElement>("ShiftProgress").style.width = Length.Percent(ratio * 100f);
    }

    public void SetShiftText(string text) {
        var root = UI.rootVisualElement;
        root.Q<Label>("TimerLabel").text = text;
    }

    public void SetLives(int lives) {
        UI = GetComponent<UIDocument>();
        var root = UI.rootVisualElement;
        root.Q<Label>("LivesValue").text = "" + lives;
    }

    public void SetScore(int score) {
        var root = UI.rootVisualElement;
        root.Q<Label>("ScoreValue").text = "" + score;
    }

    public void ClearComboSequence() {
        UI = GetComponent<UIDocument>();
        var root = UI.rootVisualElement;
        root.Q<Label>("ComboText").text = "";
    }

    public void SetComboText(string text) {
        var root = UI.rootVisualElement;
        root.Q<Label>("ComboText").text = text;
    }

    public void SetEffectiveText(string text) {
        var root = UI.rootVisualElement;
        root.Q<Label>("EffectiveText").text = text;
    }

    public void AddComboSequence(KP keyPress) {
        var root = UI.rootVisualElement;
        Label comboText = root.Q<Label>("ComboText");

        if (keyPress == KP.ShortQ) {
            comboText.text = comboText.text + " Short Q";
        } else if (keyPress == KP.LongQ) {
            comboText.text = comboText.text + " Long Q";
            // button.Q<VisualElement>("ButtonBG").layout.Set(0, 0, size.x * .5f, size.y * .5f);
        } else if (keyPress == KP.ShortW) {
            comboText.text = comboText.text + " Short W";
        } else if (keyPress == KP.LongW) {
            comboText.text = comboText.text + " Long W";
            // button.Q<VisualElement>("ButtonBG").layout.Set(0, 0, size.x * .5f, size.y * .5f);
        } else if (keyPress == KP.ShortE) {
            comboText.text = comboText.text + " Short E";
        } else if (keyPress == KP.LongE) {
            comboText.text = comboText.text + " Long E";
            // button.Q<VisualElement>("ButtonBG").layout.Set(0, 0, size.x * .5f, size.y * .5f);
        }
    }

    public void AddCombosUI(List<Combo> combos) {
        UI = GetComponent<UIDocument>();
        var root = UI.rootVisualElement;
        VisualElement comboListContainer = root.Q<VisualElement>("ComboListContainer");

        // This is jank but will work
        foreach (Combo combo in combos) {
            VisualElement container = comboContainer.Instantiate();
            container.Q<Label>("ComboName").text = combo.Name;
            VisualElement comboButtons = container.Q<VisualElement>("ComboButtons");
            foreach (KP keyPress in combo.Sequence) {
                VisualElement button = comboButton.Instantiate();
                // Vector2 size = button.Q<VisualElement>("ButtonBG").layout.size;
                if (keyPress == KP.ShortQ) {
                    button.Q<Label>("ButtonKey").text = "Q";
                    button.Q<VisualElement>("ButtonBG").style.backgroundColor = new StyleColor(new Color(1f, 1f, 1f, 1));
                } else if (keyPress == KP.LongQ) {
                    button.Q<Label>("ButtonKey").text = "Q";
                    button.Q<VisualElement>("ButtonBG").style.backgroundColor = longPressColor;
                } else if (keyPress == KP.ShortW) {
                    button.Q<Label>("ButtonKey").text = "W";
                    button.Q<VisualElement>("ButtonBG").style.backgroundColor = new StyleColor(new Color(1f, 1f, 1f, 1));
                } else if (keyPress == KP.LongW) {
                    button.Q<Label>("ButtonKey").text = "W";
                    button.Q<VisualElement>("ButtonBG").style.backgroundColor = longPressColor;
                } else if (keyPress == KP.ShortE) {
                    button.Q<Label>("ButtonKey").text = "E";
                    button.Q<VisualElement>("ButtonBG").style.backgroundColor = new StyleColor(new Color(1f, 1f, 1f, 1));
                } else if (keyPress == KP.LongE) {
                    button.Q<Label>("ButtonKey").text = "E";
                    button.Q<VisualElement>("ButtonBG").style.backgroundColor = longPressColor;
                } 
                comboButtons.Add(button);
            }

            container.Q<Label>("ComboName").text = combo.Name;
            comboListContainer.Add(container);
        }
    }

    public void SetButtonPressed(string input) {
        for (int i = 0; i < inputButtons.Length; i++) {
            if (input.ToUpper() == inputButtons[i].Q<Label>("ButtonKey").text) {
                heldButton = inputButtons[i].Q<VisualElement>("ButtonBG");
                heldButton.style.backgroundColor = new StyleColor(new Color(0.5f, 0.5f, 0.5f, 1));
            }
        }

        buttonHeld = true;
        buttonHeldTimer = 0f;
    }

    public void SetButtonReleased(string input) {
        for (int i = 0; i < inputButtons.Length; i++) {
            if (input.ToUpper() == inputButtons[i].Q<Label>("ButtonKey").text) {
                heldButton = inputButtons[i].Q<VisualElement>("ButtonBG");
                heldButton.style.backgroundColor = new StyleColor(new Color(1, 1, 1, 1));
            }
        }

        buttonHeld = false;
        buttonHeldTimer = 0f;
    }

}
