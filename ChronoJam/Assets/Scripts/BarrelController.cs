using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelController : MonoBehaviour
{
    public ScoreManager scoreManager;
    public Animator barrelAnim;
    public float speed = 2f;
    public bool moving = false;
    public int  points = 300;

    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        barrelAnim = GetComponent<Animator>();
        conveyorAnim(false);
        //pause animation until we have a contact
        barrelAnim.speed = 0;
    }

    public void MoveBarrel() {
        moving = true;
        barrelAnim.speed = 1;
        barrelAnim.Play("barrel_hit_anim");
        GameObject.Find("UI").GetComponent<UIController>().SetEffectiveText("Canned!");
        GameObject.Find("SFXplayer").GetComponent<AudioController>().playBarrelHit();
        
        conveyorAnim(true);
        
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0f);
    }

    //Upon collision with another GameObject, this GameObject will reverse direction
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "BarrelScore") {
            scoreManager.AddScore(points);
            conveyorAnim(false);
            Destroy(this.gameObject);
        }
    }

    public void conveyorAnim(bool start) {
        Debug.Log("inside conveyor anim");
        GameObject[] hConveyors = GameObject.FindGameObjectsWithTag("conveyor_h");
        if (start) {
            Debug.Log("start conveyor anim");
            foreach (GameObject hc in hConveyors) {
                var anim = hc.GetComponent<Animator>();
                anim.Play("conveyor_h_move");
                anim.speed = 1;
                
                
            }
        }
        else {
            Debug.Log("stop conveyor anim");
            foreach (GameObject hc in hConveyors) {
                var anim = hc.GetComponent<Animator>();
                anim.speed = 0;
            }
        }
    }
}
