using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftManager : MonoBehaviour
{
    public EnemySpawner enemySpawner;
    public UIController uiController;
    public float shiftTimer;
    public float shiftLength;
    public float spawnTimer;
    public float enemySpawnLength;
    public int shiftNumber;

    // Start is called before the first frame update
    void Start()
    {
        uiController = GameObject.Find("UI").GetComponent<UIController>();
        enemySpawner = GameObject.Find("EnemySpawner").GetComponent<EnemySpawner>();
        enemySpawner.Spawn(0);
    }

    // Update is called once per frame
    void Update()
    {
        shiftTimer += Time.deltaTime;
        spawnTimer += Time.deltaTime;
        uiController.SetShiftProgress(shiftTimer / shiftLength);
        uiController.SetShiftText("Shift #" + shiftNumber);

        if (shiftTimer >= shiftLength) {
            shiftNumber++;
            shiftTimer = 0f;
            uiController.SetEffectiveText("New Shift!");
            GameObject.Find("SFXplayer").GetComponent<AudioController>().playShiftChange();

            // Spawn enemies faster?
            enemySpawnLength = enemySpawnLength * .9f;
        }

        if (spawnTimer >= enemySpawnLength) {
            int i = Random.Range(0, shiftNumber + 1);
            enemySpawner.Spawn(i);
            spawnTimer = 0f;
        }
    }
}
