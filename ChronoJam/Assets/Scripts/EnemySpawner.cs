using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemyPrefabs;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spawn(int i) {
        Instantiate(enemyPrefabs[i % enemyPrefabs.Length], this.transform.position, this.transform.rotation);
    }
}
