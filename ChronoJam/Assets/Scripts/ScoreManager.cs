using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public UIController uiController;
    public GameObject mainMenu;
    int score;
    public int lives = 5;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        mainMenu = GameObject.Find("MainMenu");
        uiController = GameObject.Find("UI").GetComponent<UIController>();
        uiController.SetLives(lives);
    }

    public void AddScore(int points) {
        score += points;
        uiController.SetScore(score);
    }

    public void LoseLife() {
        lives -= 1;
        uiController.SetLives(lives);
        if (lives == 0) {
            Time.timeScale = 0;
            mainMenu.SetActive(true);
            mainMenu.GetComponent<MainMenuControl>().ShowGameOverText(score);
            GameObject.Find("InputManager").GetComponent<InputManager>().SetInputEnable(false);
        }
    }

    public void GameRestart() {
        lives = 5;
        score = 0;
        var shiftManager = GameObject.Find("ShiftManager").GetComponent<ShiftManager>();
        shiftManager.shiftNumber = 1;
        shiftManager.shiftTimer = 0;
        shiftManager.enemySpawnLength = 5f;
        uiController.SetLives(lives);
        uiController.SetScore(score);
    }
}
