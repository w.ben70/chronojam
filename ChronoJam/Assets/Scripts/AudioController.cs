using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{  
    public AudioClip critHit;
    public AudioClip badHit;
    public AudioClip normalHit;
    public AudioClip barrelHit;
    public AudioClip miss;
    public AudioClip shiftChange;
    public AudioClip comboTimeout;

    AudioSource sfxPlayer;
    // Start is called before the first frame update
    void Start()
    {
        sfxPlayer = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playBarrelHit() {
        sfxPlayer.PlayOneShot(barrelHit);
    }

    public void playCritHit() {
        sfxPlayer.PlayOneShot(critHit);
    }

    public void playBadHit() {
        sfxPlayer.PlayOneShot(badHit);
    }

    public void playNormalHit() {
        sfxPlayer.PlayOneShot(normalHit);
    }

    public void playMiss() {
        sfxPlayer.PlayOneShot(miss);
    }

    public void playShiftChange() {
        sfxPlayer.PlayOneShot(shiftChange);
    }

    public void playComboTimeout() {
        sfxPlayer.PlayOneShot(comboTimeout);
    }
}
