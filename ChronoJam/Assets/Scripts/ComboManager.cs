using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Short for KeyPressType
public enum KP
{
    ShortQ,
    LongQ,
    ShortW,
    LongW,
    ShortE,
    LongE
}

public struct Combo {
    public List<KP> Sequence;
    public string Name;
    public float Damage;
    public float yVelocity;
    public float xVelocity;
    // TODO - Reference GUI / Gameobjects
};

// TODO - Super-set combos (when one combo is a bigger version of a smaller combo)
public class ComboManager : MonoBehaviour
{
    // List of keyPressTypes accruing to the current combo
    List<KP> comboSequence;

    // List of all the combos, populated in start
    // Could use some crazy datastructure for pair programming
    List<Combo> combos;

    // How long should a longPress be?
    public float longPressTime = 1;

    // How long until a combo times out?
    public float comboTimeoutSeconds = 2;

    // How long since the last keyPress was added to the combo?
    public float comboTimer = 0;

    AudioController sfxController;

    // On the start of a keyPress, reset the comboTimer
    public void resetComboTimer() {
        comboTimer = 0;
    }

    public void AddInput(string code, float time)
    {
        Debug.Log("AddInput code=" + code + " time=" + time);
        KP keyPress = KP.LongQ;;

        if (comboSequence.Count == 0) {
            GameObject.Find("UI").GetComponent<UIController>().ClearComboSequence();
        }

        if (time > longPressTime) {
            switch (code) {
                case "q":
                    keyPress = KP.LongQ;
                    break;
                case "w":
                    keyPress = KP.LongW;
                    break;
                case "e":
                    keyPress = KP.LongE;
                    break;
            }
        } else {
            switch (code) {
                case "q":
                    keyPress = KP.ShortQ;
                    break;
                case "w":
                    keyPress = KP.ShortW;
                    break;
                case "e":
                    keyPress = KP.ShortE;
                    break;
            }
        }

        comboSequence.Add(keyPress);
        GameObject.Find("UI").GetComponent<UIController>().AddComboSequence(keyPress);

        // If no combos match, reset the combo
        if(!matchComboAndExecute()) {
            resetComboSequence();
        // We're working towards a combo
        } else {
            resetComboTimer();
        }

    }

    // O(N^2) search through combos b/c I don't care about efficiency
    private bool matchComboAndExecute() {
        bool matchedCombo = false;
        foreach (Combo combo in combos) {
            // Check to see if the sequences are identical
            bool match = true;
            for (int i = 0; i < comboSequence.Count; i++) {
                if (combo.Sequence[i] != comboSequence[i]) {
                    match = false;
                    break;
                }
            }

            // This means we are on track for one or more combos, not necessarily executed it
            if (match) {
                Debug.Log("Matching " + combo.Name + " " + comboSequence.Count + "/" + combo.Sequence.Count + " steps");
                matchedCombo = true;
            }

            // Not matched // Not the same # of inputs, skip
            if (combo.Sequence.Count != comboSequence.Count) {
                continue;
            }

            if (match) {
                Debug.Log("Executing combo " + combo.Name);
                // Have to execute
                resetComboSequence();
                GameObject.Find("UI").GetComponent<UIController>().SetComboText(combo.Name + "!!!!!");
                findAndAttack(combo);
                return matchedCombo;
            }
        }

        return matchedCombo;
    }

    private void findAndAttack(Combo c) {
        bool hit = false;
        EnemyController[] enemies = (EnemyController[]) GameObject.FindObjectsOfType(typeof(EnemyController));
        foreach (EnemyController enemy in enemies) {
            if (enemy.attackable) {
                enemy.DealDamage(c);
                hit = true;
            }
        }

        if (!hit) {
            GameObject.Find("UI").GetComponent<UIController>().SetEffectiveText("Miss!");
            GameObject.Find("SFXplayer").GetComponent<AudioController>().playMiss();
        }

        GameObject.Find("char_idle").GetComponent<Animator>().Play(c.Name);
    }

    private void resetComboSequence() {
        comboSequence.Clear();
        comboTimer = 0;
        Debug.Log("resetComboSequence");
        GameObject.Find("UI").GetComponent<UIController>().ClearComboSequence();
        GameObject.Find("UI").GetComponent<UIController>().SetComboProgress(0);
        GameObject.Find("UI").GetComponent<UIController>().HideCombo();
    }

    // Start is called before the first frame update
    void Start()
    {
        combos = new List<Combo>();
        comboSequence = new List<KP>();
        sfxController = GameObject.Find("SFXplayer").GetComponent<AudioController>();

        Combo jab;
        jab.Sequence = new List<KP> { KP.ShortQ, KP.ShortQ };
        jab.Name = "Jab";
        jab.Damage = 15f;
        jab.xVelocity = 3f;
        jab.yVelocity = 3f;
        combos.Add(jab);

        Combo kick;
        kick.Sequence = new List<KP> { KP.ShortE, KP.ShortE, KP.ShortW };
        kick.Name = "Kick";
        kick.Damage = 35f;
        kick.xVelocity = 8f;
        kick.yVelocity = 1f;
        combos.Add(kick);

        Combo upperCut;
        upperCut.Sequence = new List<KP> { KP.LongQ, KP.ShortE };
        upperCut.Name = "Upper Cut";
        upperCut.Damage = 50f;
        upperCut.xVelocity = 3f;
        upperCut.yVelocity = 7f;
        combos.Add(upperCut);

        Combo lowKick;
        lowKick.Sequence = new List<KP> { KP.LongW };
        lowKick.Name = "Low Kick";
        lowKick.Damage = 30f;
        lowKick.xVelocity = 1f;
        lowKick.yVelocity = 5f;
        combos.Add(lowKick);

        Combo ultimatePunch;
        ultimatePunch.Sequence = new List<KP> { KP.LongQ, KP.LongW, KP.LongE, KP.ShortQ };
        ultimatePunch.Name = "Ultimate Punch";
        ultimatePunch.Damage = 100f;
        ultimatePunch.xVelocity = 6f;
        ultimatePunch.yVelocity = 6f;
        combos.Add(ultimatePunch);

        GameObject.Find("UI").GetComponent<UIController>().ClearComboSequence();
        GameObject.Find("UI").GetComponent<UIController>().AddCombosUI(combos);
    }

    // Update is called once per frame
    void Update()
    {
        // If an comboSequence combo is going
        if (comboSequence.Count > 0) {
            // Combo timed out
            if (comboTimer > comboTimeoutSeconds) {
                Debug.Log("Combo timed out. comboTimer=" + comboTimer + " comboTimeoutSeconds=" + comboTimeoutSeconds);
                resetComboSequence();
                GameObject.Find("UI").GetComponent<UIController>().SetComboText("Too Slow!");
                sfxController.playComboTimeout();
            } else {
                comboTimer += Time.deltaTime;
                GameObject.Find("UI").GetComponent<UIController>().SetComboProgress(comboTimer / comboTimeoutSeconds);
            }
        }

    }
}
