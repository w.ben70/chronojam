using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelSpawner : MonoBehaviour
{
    public GameObject barrel;
    public GameObject spawnPoint;
    public float spawnRange;

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Upon collision with another GameObject, this GameObject will reverse direction
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Barrel") {
            Spawn();
        }
    }

    private void Spawn() {
        Vector2 pos = spawnPoint.transform.position;
        pos.x += Random.Range(0f, spawnRange);
        Instantiate(barrel, pos, spawnPoint.transform.rotation);
    }
}
